---
name: 'Xiaomi Mi Note 2'
comment: 'wip'
deviceType: 'phone'
maturity: .05

externalLinks:
  -
    name: 'Forum Post'
    link: 'https://forums.ubports.com/topic/3947/xiaomi-mi-note-2-scorpio'
    icon: 'yumi'
---

### Working

 - Audio (partially, headphones don't work)
 - Calling
 - SMS
 - Wi-Fi
 - Wi-Fi Hotspot
 - Vibration
 - Orientation sensor
 - Proximity Sensor

### Not working

 - Bluetooth
 - Camera
 - Flashlight
 - GPS
