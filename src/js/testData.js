module.exports = {
  runBefore: beforeElaboration,
  runAfter: afterElaboration
};

const portStatus = require("../../data/portStatus.json");
const deviceInfo = require("../../data/deviceInfo.json");
const progressStages = require("../../data/progressStages.json");

const dataUtils = require("./dataUtils.js");

function beforeElaboration(device, installerData) {
  if (device.portStatus) {
    findMissingFeatures(device);
  } else {
    console.log(
      "\x1b[33m%s\x1b[0m",
      "[" +
        device.fileInfo.name +
        "] " +
        "Using maturity as fallback for: " +
        device.name
    );
  }

  if (device.deviceInfo) {
    findMissingDeviceInfo(device);
  }
}

function afterElaboration(device, installerData) {
  if (device.portStatus) {
    nextStageRequirements(device);
  }
}

/* Helpers */
function convertToFeatureMatrix(data) {
  let output = [];
  for (let feature of data) {
    let category = output.find((el) => el.categoryName == feature.category);

    if (!category) {
      category = {
        categoryName: feature.category,
        features: []
      };
      output.push(category);
    }
    category.features.push({
      name: feature.name,
      value: "-"
    });
  }
  return output;
}

/* Functions */

function findMissingFeatures(device, fail = false) {
  let notFound = []; // Terminal logs
  let notFoundTable = []; // Displayed at /device/codename/developer

  // Generate list of missing data
  dataUtils.forEachFeature(device, function (feature, category) {
    if (!dataUtils.getFeatureById(category, feature.id)) {
      notFound.push({
        id: feature.id,
        name: feature.name,
        category: category.categoryName
      });
    }
  });

  // Display missing feature warning
  if (notFound.length > 0) {
    console.log(
      "\x1b[33m%s\x1b[0m",
      "[" +
        device.fileInfo.name +
        "] " +
        device.name +
        " has missing features in feature matrix.",
      "\nThe following features are missing:"
    );

    for (let el of notFound) {
      console.log(el.category + " : " + el.name);
    }

    if (fail) {
      process.exit(2);
    }
  }

  // Add data to website to display at /device/codename/developer
  device.portStatusMissing = convertToFeatureMatrix(notFound);
}

function findMissingDeviceInfo(device, fail = false) {
  // Reuse the device specification component
  let notFound = [];

  // Generate list of missing data
  for (let specification of deviceInfo) {
    if (!dataUtils.getSpecificationById(device, specification.id)) {
      notFound.push({
        id: specification.id,
        name: specification.name,
        category: "Missing device info"
      });
    }
  }

  // Display missing data warning
  if (notFound.length > 0 && fail) {
    console.log(
      "\x1b[33m%s\x1b[0m",
      "[" +
        device.fileInfo.name +
        "] " +
        device.name +
        " has missing data in device info.",
      "\nThe following data is missing:"
    );

    for (let el of notFound) {
      console.log(el.name);
    }

    process.exit(2);
  }

  // Add data to website to display at /device/codename/developer
  device.deviceInfoMissing = convertToFeatureMatrix(notFound);
}

function nextStageRequirements(device) {
  let missing = []; // Terminal logs

  let currentStage = device.progressStage,
    nextStage =
      progressStages[progressStages.findIndex((el) => el == currentStage) + 1];

  dataUtils.forEachFeature(device, function (feature, category) {
    if (feature.stage == nextStage) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);
      if (graphQlFeature && graphQlFeature.value != "+") {
        missing.push({
          id: feature.id,
          name: feature.name,
          category: category.categoryName
        });
      }
    }
  });

  device.nextProgressStage = nextStage;
  device.nextStageRequirements = convertToFeatureMatrix(missing);
}
